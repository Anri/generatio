# [GeneRatio](https://twitter.com/Mylloon/status/1699226564519514358)

- Installe les dépendances : `pip install -r requirements.txt`
- Remplir un .env avec les champs suivant :

```env
SPOTIPY_CLIENT_ID="XXXXX"
SPOTIPY_CLIENT_SECRET="XXXXX"
SPOTIPY_REDIRECT_URI="http://127.0.0.1:9090"
```

> Les données devraient correspondre avec tes infos persos
> de [Spotify Dashboard](https://developer.spotify.com/dashboard)

Suis les étapes du script, à la fin, il devrait y avoir un resultat.png.
